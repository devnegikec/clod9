<?php

/**
 * This is the model class for table "person".
 *
 * The followings are the available columns in table 'person':
 * @property string $id
 * @property string $name
 * @property string $profession
 * @property string $dob
 * @property string $biography
 * @property string $create
 * @property string $updated
 * @property integer $active
 * @property string $user_id
 *
 * The followings are the available model relations:
 * @property Movie[] $movies
 * @property User $user
 * @property PersonComment[] $personComments
 */
class Person extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Person the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'person';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, user_id', 'required'),
			array('active', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			array('user_id', 'length', 'max'=>10),
			array('profession, dob, biography, create, updated', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, profession, dob, biography, create, updated, active, user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'movies' => array(self::MANY_MANY, 'Movie', 'movies_person(person_id, movie_id)'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'personComments' => array(self::HAS_MANY, 'PersonComment', 'person_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'profession' => 'Profession',
			'dob' => 'Dob',
			'biography' => 'Biography',
			'create' => 'Create',
			'updated' => 'Updated',
			'active' => 'Active',
			'user_id' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('profession',$this->profession,true);
		$criteria->compare('dob',$this->dob,true);
		$criteria->compare('biography',$this->biography,true);
		$criteria->compare('create',$this->create,true);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('user_id',$this->user_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}