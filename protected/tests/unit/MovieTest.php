<?php
class MovieTest extends CDbTestCase
{
	public function testCRUD()
	{
		$newMovie =new Movie;
		$newMovieTitle = 'Test Movie 1';
		$newMovie->setAttributes(
		array(
		'title' => $newMovieTitle,
		'releasedate' => '2012-11-01',
		'summary' => 'This is summary',
		'story' => 'This is story',
		'created' => '2010-01-01 00:00:00',
		'user_id' => 1,
		)
		);
		$this->assertTrue($newMovie->save(false));
		
		//READ back the newly created Movie
		$retrievedMovie = Movie::model()->findByPk($newMovie->id);
		$this->assertTrue($retrievedMovie instanceof Movie);
		$this->assertEquals($newMovieTitle,$retrievedMovie->title);
	}
}